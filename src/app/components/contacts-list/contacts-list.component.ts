import { Component, OnInit } from '@angular/core';
import { ContactosService } from 'src/app/services/contactos.service';

@Component({
  selector: 'app-contacts-list',
  templateUrl: './contacts-list.component.html',
  styleUrls: ['./contacts-list.component.scss'],
})
export class ContactsListComponent implements OnInit {
  Contacts: any = [];
  loading: boolean = true;
  constructor(public api: ContactosService) {}

  ngOnInit(): void {
    this.loadContacts();
  }

  loadContacts() {
    return this.api.getContacts().subscribe((data: {}) => {
      this.Contacts = data;
      this.loading = false;
    });
  }

  deleteContact(id: string) {
    if (window.confirm('Are you sure, you want to delete?')) {
      this.api.deleteContact(id).subscribe((data) => {
        this.loadContacts();
      });
    }
  }
}
