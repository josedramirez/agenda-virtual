import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ContactosService } from 'src/app/services/contactos.service';

@Component({
  selector: 'app-edit-contact',
  templateUrl: './edit-contact.component.html',
  styleUrls: ['./edit-contact.component.scss'],
})
export class EditContactComponent implements OnInit {
  id = this.actRoute.snapshot.params['id'];
  contactDetails: FormGroup;
  submitted: boolean;
  loading: boolean;
  constructor(
    public api: ContactosService,
    public actRoute: ActivatedRoute,
    public router: Router,
    private fb: FormBuilder,
    private toastr: ToastrService
  ) {
    this.contactDetails = this.fb.group({
      name: ['', Validators.required],
      last_name: ['', Validators.required],
      phone: ['', [Validators.required, Validators.pattern('^[0-9]*$')]],
    });
    this.submitted = false;
    this.loading = false;
  }

  ngOnInit(): void {
    this.getContact(this.id);
  }
  getContact(id: string) {
    this.api.getContact(id).subscribe((data: {}) => {
      console.log(data);
      this.patchContactValues(data);
    });
  }
  updateContact() {
    if (this.contactDetails.invalid) {
      return;
    }
    this.loading = true;

    const contact: any = {
      name: this.contactDetails.value.name,
      last_name: this.contactDetails.value.last_name,
      phone: this.contactDetails.value.phone,
    };
    if (window.confirm('Are you sure, you want to update?')) {
      this.api.updateContact(this.id, contact).subscribe((data) => {
        this.toastr.info(
          'El contacto fue modificado con exito',
          'Contacto modificado',
          {
            positionClass: 'toast-bottom-right',
          }
        );
        this.loading = false;
        this.router.navigate(['/contact-list']);
      });
    }
  }
  patchContactValues(data: any) {
    this.contactDetails.patchValue({
      name: data.name,
      last_name: data.last_name,
      phone: data.phone,
    });
  }
  numericOnly(event: any): boolean {
    let pattern = /^([0-9])$/;
    let result = pattern.test(event.key);
    return result;
  }

  get name() {
    return this.contactDetails.get('name');
  }
  get last_name() {
    return this.contactDetails.get('last_name');
  }
  get phone() {
    return this.contactDetails.get('phone');
  }
}
