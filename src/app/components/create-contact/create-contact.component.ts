import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ContactosService } from 'src/app/services/contactos.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-create-contact',
  templateUrl: './create-contact.component.html',
  styleUrls: ['./create-contact.component.scss'],
})
export class CreateContactComponent implements OnInit {
  contactDetails: FormGroup;
  submitted: boolean;
  loading: boolean;
  constructor(
    public api: ContactosService,
    public router: Router,
    private fb: FormBuilder,
    private toastr: ToastrService
  ) {
    this.contactDetails = this.fb.group({
      name: ['', Validators.required],
      last_name: ['', Validators.required],
      phone: ['', [Validators.required, Validators.pattern('^[0-9]*$')]],
    });
    this.submitted = false;
    this.loading = false;
  }

  ngOnInit(): void {}

  addContact() {
    this.submitted = true;

    if (this.contactDetails.invalid) {
      return;
    }
    const contact: any = {
      name: this.contactDetails.value.name,
      last_name: this.contactDetails.value.last_name,
      phone: this.contactDetails.value.phone,
    };

    this.loading = true;
    this.api.createContact(contact).subscribe(
      (data: {}) => {
        this.toastr.success(
          'El contacto fue registrado con exito!',
          'Contacto Registrado',
          {
            positionClass: 'toast-bottom-right',
          }
        );
        this.loading = false;
        this.router.navigate(['/contact-list']);
      },
      (err) => {
        console.log(err);
        this.loading = false;
      }
    );
  }
  numericOnly(event: any): boolean {
    let pattern = /^([0-9])$/;
    let result = pattern.test(event.key);
    return result;
  }

  get name() {
    return this.contactDetails.get('name');
  }
  get last_name() {
    return this.contactDetails.get('last_name');
  }
  get phone() {
    return this.contactDetails.get('phone');
  }
}
