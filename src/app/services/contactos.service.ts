import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Contact } from '../shared/contact';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ContactosService {
  private REST_API_SERVER = 'http://localhost:3000';
  constructor(private http: HttpClient) {}
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };
  getContacts(): Observable<Contact> {
    return this.http
      .get<Contact>(this.REST_API_SERVER + '/contacts')
      .pipe(retry(1), catchError(this.handleError));
  }

  getContact(id: string): Observable<Contact> {
    return this.http
      .get<Contact>(this.REST_API_SERVER + '/contacts/' + id)
      .pipe(retry(1), catchError(this.handleError));
  }

  createContact(contact: any): Observable<Contact> {
    return this.http
      .post<Contact>(
        this.REST_API_SERVER + '/contacts',
        JSON.stringify(contact),
        this.httpOptions
      )
      .pipe(retry(1), catchError(this.handleError));
  }

  updateContact(id: string, contact: any): Observable<Contact> {
    return this.http
      .put<Contact>(
        this.REST_API_SERVER + '/contacts/' + id,
        JSON.stringify(contact),
        this.httpOptions
      )
      .pipe(retry(1), catchError(this.handleError));
  }

  deleteContact(id: string) {
    return this.http
      .delete<Contact>(
        this.REST_API_SERVER + '/contacts/' + id,
        this.httpOptions
      )
      .pipe(retry(1), catchError(this.handleError));
  }

  handleError(error: {
    error: { message: string };
    status: any;
    message: any;
  }) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
}
