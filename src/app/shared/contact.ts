export class Contact {
  id!: string;
  name!: string;
  last_name!: string;
  phone!: number;
}
